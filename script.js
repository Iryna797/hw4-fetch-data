// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
    
// AJAX (асинхронний JavaScript і XML) - це технологія, що дозволяє взаємодіяти з сервером без перезавантаження сторінки.


const insertList = document.querySelector(".movie-list");
const url = `https://ajax.test-danit.com/api/swapi/films`;

function state(res) {
  if (!res.ok) {
    throw new Error("The server returned an error response");
  } else {
    return res.json();
  }
}

function renderName(data) {
  const list = data.map(({ episodeId, name, openingCrawl }) => {
    return `<li class='movie-name'><b>Episode</b>: ${episodeId} <br><b>Name</b>: ${name} <br><b>Crawl</b>: ${openingCrawl}</li>`;
  });
  insertList.insertAdjacentHTML("afterbegin", list.join(""));
}

function renderCharacters(data, i) {
  const spinners = document.querySelectorAll(".spinner");
  const movieItem = document.querySelectorAll(".movie-name");
  const ul = document.createElement("ul");
  const p = document.createElement("p");

  setTimeout(() => {
    if (spinners[i]) {
        spinners[i].classList.add("no-show");
      }
    p.innerHTML = "<b>Characters:</b>";
    const listChar = data.map(({ name }) => `<li>${name}</li>`);
    ul.insertAdjacentHTML("afterbegin", listChar.join(""));
    movieItem[i].append(p);
    movieItem[i].append(ul);
  }, 1000);
}

fetch(url)
  .then((response) => state(response))
  .then((data) => {
    renderName(data);

    data.forEach((item, index) => {
      const promises = item.characters.map((i) =>
        fetch(i).then((response) => state(response))
      );
      Promise.all(promises).then((data) => {
        renderCharacters(data, index);
      });
    });
  })
  .catch((error) => {
    console.error("There was a problem:", error);
  });



